﻿
using Demo.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demo.DAL.Repositories
{
    public class Table1Repository
    {
        public IEnumerable<Table1> Get()
        {
            using (SqlConnection conn = new SqlConnection(
                ConfigurationManager.ConnectionStrings["default"].ConnectionString
            ))
            {
                conn.Open();
                SqlCommand cmd = conn.CreateCommand();
                cmd.CommandText = "SELECT * FROM Table1";
                SqlDataReader r = cmd.ExecuteReader();
                while(r.Read())
                {
                    yield return new Table1
                    {
                        Id = (int)r["id"],
                        Field1 = (string)r["Field1"],
                        Field2 = (DateTime)r["Field2"],
                    };
                }
            }
        }
    }
}
