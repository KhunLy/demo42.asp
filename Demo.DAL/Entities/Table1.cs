﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demo.DAL.Entities
{
    public class Table1
    {
        public int Id { get; set; }
        public string Field1 { get; set; }
        public DateTime Field2 { get; set; }
    }
}
