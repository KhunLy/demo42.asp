﻿using Demo.ASP.Models;
using Demo.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Demo.ASP.Mappers
{
    public static class MapperExtentions
    {
        public static Table1DTO ToModel(this Table1 entity)
        {
            return new Table1DTO
            {
                Id = entity.Id,
                Field1 = entity.Field1,
                Field2 = entity.Field2
            };
        }
    }
}