﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Demo.ASP.Models
{
    public class Table1DTO
    {
        public int Id { get; set; }
        public string Field1 { get; set; }
        public DateTime Field2 { get; set; }
    }
}