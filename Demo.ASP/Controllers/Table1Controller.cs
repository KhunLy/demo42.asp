﻿using Demo.ASP.Mappers;
using Demo.ASP.Models;
using Demo.DAL.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Demo.ASP.Controllers
{
    public class Table1Controller : ApiController
    {
        public IEnumerable<Table1DTO> Get()
        {
            Table1Repository repo = new Table1Repository();
            return repo.Get().Select(x => x.ToModel());
        }
    }
}
